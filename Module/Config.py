import os

from Public.XmlParser import XmlParser

index = os.getcwd().find('automation_test')
rootPath = os.getcwd()[0:index + 15]
xmlObject = XmlParser(rootPath + '/Config.xml')
Env = xmlObject.get_text_value('env')
browser = xmlObject.get_text_value('browser')
auctionId = xmlObject.get_text_value('auctionId')
tradeId = xmlObject.get_text_value('tradeId')
emulateMobile = xmlObject.get_text_value('emulateMobile')
