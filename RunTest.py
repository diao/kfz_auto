import os
import time
import unittest
import TestList
from Public.HTMLTestRunner import HTMLTestRunner
from Public.Toolkit import Toolkit

if __name__ == '__main__':
    testSuite = unittest.TestSuite()

    # 书店
    # testSuite.addTests(TestList.BookStore)

    # 拍卖
    # testSuite.addTests(TestList.Auction)

    # 资金
    # testSuite.addTests(TestList.Fund)

    # 买家中心
    # testSuite.addTests(TestList.BuyerCenter)

    # 卖家中心
    # testSuite.addTests(TestList.SellerCenter)

    # 书店订单
    # testSuite.addTests(TestList.BookOrder)

    # 拍卖订单
    # testSuite.addTests(TestList.AuctionOrder)

    # 信誉评价
    # testSuite.addTests(TestList.Evaluation)

    # 搜索
    # testSuite.addTests(TestList.Search)

    folder = './Log/' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + '/'
    os.makedirs(folder, exist_ok=True)
    with open(folder + 'result_' + Toolkit.get_random_value() + '.html', 'w', encoding='utf-8') as f:
        HTMLTestRunner(stream=f, title='Test Report', description=u'').run(testSuite)
