from MobileTest.Page.MSitePage import MSitePage
from Module import Uri


class MSiteAction:
    def __init__(self):
        self.page = MSitePage()

    def search(self, search_content, search_type):
        self.page.go_to(Uri.MSearchPage)
        if search_type == 'book':
            self.page.book_search_tab.click()
        if search_type == 'auction':
            self.page.auction_search_tab.click()
        if search_type == 'shop':
            self.page.shop_search_tab.click()
        self.page.search_txt.send_keys(search_content)
        self.page.search_submit_btn.click()

    def get_search_result(self, search_type):
        results = []
        if search_type == 'book':
            for result in self.page.book_search_result:
                results.append(result.text)
        if search_type == 'auction':
            for result in self.page.auction_search_result:
                results.append(result.text)
        if search_type == 'shop':
            for result in self.page.shop_search_result:
                results.append(result.text)
        return results
