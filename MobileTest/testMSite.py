import unittest
from MobileTest.Action.MSiteAction import MSiteAction


class MSiteTestCase(unittest.TestCase):
    def setUp(self):
        self.action = MSiteAction()

    def tearDown(self):
        self.action.page.quit()

    def testSearch(self):
        self.action.search('测试', 'book')
        self.verify_search_result('book')

        self.action.search('测试', 'auction')
        self.verify_search_result('auction')

        self.action.search('测试', 'shop')
        self.verify_search_result('shop')

    def verify_search_result(self, search_type):
        results = self.action.get_search_result(search_type)
        if len(results) != 0:
            for result in results:
                self.assertIn('测试', result)
        else:
            self.fail('No search result!!!')
