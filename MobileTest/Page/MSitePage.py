from Public.Browser import Browser


class MSitePage(Browser):
    @property
    def book_search_tab(self):
        return self.browser.find_element_by_css_selector('a[data-area="shop"]')

    @property
    def auction_search_tab(self):
        return self.browser.find_element_by_css_selector('a[data-area="pm"]')

    @property
    def shop_search_tab(self):
        return self.browser.find_element_by_css_selector('a[data-area="dianpu"]')

    @property
    def search_txt(self):
        return self.browser.find_element_by_id('search-kw')

    @property
    def search_submit_btn(self):
        return self.browser.find_element_by_id('submitBtn')

    @property
    def book_search_result(self):
        return self.browser.find_element_by_id('content').find_elements_by_css_selector('p[class="book-box-con-name"]')

    @property
    def auction_search_result(self):
        return self.browser.find_element_by_id('result_search_con').find_elements_by_css_selector(
            'p[class="auctioning_title black_color"]')

    @property
    def shop_search_result(self):
        return self.browser.find_element_by_css_selector('div[class="content"]').find_elements_by_css_selector(
            'div[class="title"]')
